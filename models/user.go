package models

import (
	. "strings"
	"time"

	"github.com/google/uuid"
)

// Abstration of User Object
type User struct {
	UserID         string
	Username       string
	IsActive       bool
	Email          string
	InvitationSend int
	CompanyName    string
	CreatedAt      string //time.Time
	ModifiedAt     string //time.Time
}

// Function set user
func (user *User) SetUserID() {
	user.UserID = uuid.New().String()
}

// Function set created
func (user *User) SetCreatedAt() {
	user.CreatedAt = time.Now().Format("2006-01-02T15:04:05-0700")
}

// Function set modified
func (user *User) SetModifiedAt() {
	user.ModifiedAt = time.Now().Format("2006-01-02T15:04:05-0700")
}

// Function set user active
func (user *User) SetIsActive() {
	user.IsActive = true
}

// Function set invitation send
func (user *User) SetInvitationSend() {
	user.InvitationSend = 0
}

// Function set company name
func (user *User) SetCompanyName(email string) {
	var splitEmail = Split(Split(email, "@")[1], ".")[0]
	user.CompanyName = splitEmail
}
