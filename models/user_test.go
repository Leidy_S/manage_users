package models

import (
	"errors"
	"github.com/google/uuid"
	"projectT/models"
	"testing"

	"github.com/stretchr/testify/requiere"
)


func TestCreateUser(t *testing.T) {

	var user, _ = CreateUser("nick","nick@mycompany.com")

	var expectedUser = &models.User {
		
		username string = "Bobby"
		email    string = "b@gmail.com"
		companyID string = "7489eue"
		isActive       bool   = true
		userID         string = "q5517h"
		invitationSend int    = 0
		create         string = time.Now().Format("2006-01-02T15:04:05-0700")
		modificate     string = time.Now().Format("2006-01-02T15:04:05-0700")*/
	}

	test_1 := requiere.New(t)
	if username == "" || email == "" {
		return (nil), errors.New("Erro empty")
	}

	user := new(models.User)
	user.SetUserID()
	user.SetCreatedAt()
	user.SetCompanyName(email)
	user.SetModifiedAt()
	user.SetIsActive()
	user.Username = username
	user.Email = email

	return user, nil
}
