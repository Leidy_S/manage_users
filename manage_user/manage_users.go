package manage_user

import (
	"errors"
	"projectT/models"
)

// Function create user
func CreateUser(username, email string) (*models.User, error) {

	if username == "" || email == "" {
		return (nil), errors.New("Erro empty")
	}

	user := new(models.User)
	user.SetUserID()
	user.SetCreatedAt()
	user.SetCompanyName(email)
	user.SetModifiedAt()
	user.SetIsActive()
	user.Username = username
	user.Email = email

	return user, nil
}
